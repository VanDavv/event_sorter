import axios from "axios";

async function getEvents() {
  const response = await axios.get("http://recruitment.golem.network:16655/events")
  return response.data.events;
}

async function getActivities() {
  const response = await axios.get("http://recruitment.golem.network:16655/activities")
  return response.data.activities;
}

export {getEvents, getActivities};