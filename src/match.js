import _ from 'lodash';
export default function matchEventsToActivities(events, activities) {
  const events_sorted = _.sortBy(events.map(event => ({...event, date: new Date(event.date), added: false})), 'date');
  const activities_sorted = _.sortBy(
    activities.map(activity => ({...activity, start: new Date(activity.start), end: new Date(activity.end), matches: []})),
    'start'
  );
  for (const activity of activities_sorted) {
    let eventStartIdx = 0;
    for (let i = eventStartIdx; i < events_sorted.length; i++) {
      const event = events_sorted[i];
      if(event.date < activity.start) {
        eventStartIdx = i;
      } else if (event.date > activity.end) {
        break;
      } else {
        activity.matches.push(event);
        event.added = true;
      }
    }
  }

  const matches = activities_sorted.map(activity => ({...activity, match_count: activity.matches.length}));
  const orphans = events_sorted.filter(event => !event.added);

  return [matches, orphans];
}