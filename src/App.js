import {Layout, Table} from 'antd';
import React, {useEffect, useState} from 'react';
import './App.css';
import _ from 'lodash';
import {getEvents, getActivities} from "./request";
import matchEventsToActivities from "./match";

const { Header, Content, Footer } = Layout;

const event_columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
    render: date => <p>{date.toLocaleString()}</p>,
    sorter: (a, b) => a.date.getTime() - b.date.getTime(),
  },
]

const activity_columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    sorter: (a, b) => a.id - b.id,
  },
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Start',
    dataIndex: 'start',
    render: date => <p>{date.toLocaleString()}</p>,
    sorter: (a, b) => a.start.getTime() - b.start.getTime(),
  },
  {
    title: 'End',
    dataIndex: 'end',
    render: date => <p>{date.toLocaleString()}</p>,
    sorter: (a, b) => a.end.getTime() - b.end.getTime(),
  },
  {
    title: 'Match count',
    dataIndex: 'match_count',
    sorter: (a, b) => a.match_count - b.match_count,
  },
]

const App = () => {
  const [orphans, setOrphans] = useState(null)
  const [activities, setActivities] = useState(null)
  useEffect(() => {
    if(!orphans || !activities) {
      const fetchData = async () => {
        const events = await getEvents();
        const raw_activities = await getActivities();
        const result = matchEventsToActivities(events, raw_activities);
        setActivities(result[0])
        setOrphans(result[1])
      }
      fetchData().catch(console.error);
    }
  }, [orphans, activities])

  return (
    <Layout className="layout">
      <Header>
        <div className="logo" />
      </Header>
      <Content className="content">
        <div className="site-layout-content">
          {
            orphans && <div>
              <p>Length of orphans: {orphans.length}</p>
              <Table dataSource={orphans} columns={event_columns} />
            </div>
          }
          {
            activities && <div>
              <p>Length of activities: {activities.length} Total matched: {_.sumBy(activities, "match_count")}</p>
              <Table
                dataSource={activities}
                rowKey="id"
                columns={activity_columns}
                expandable={{
                  expandedRowRender: activity => <Table dataSource={activity.matches} columns={event_columns} />,
                  rowExpandable: activity => activity.match_count > 0,
                }}
              />
            </div>
          }
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>Łukasz Piłatowski ©2022 Created by Łukasz Piłatowski</Footer>
    </Layout>
  );
}

export default App;